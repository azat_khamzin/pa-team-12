# for clear bd
with open('text_for_db.sql', 'w') as f:
    f.write('')


with open('text_for_db.sql', 'w') as file:
    file.write("create table db_leaders (\n"
               "    id BIGSERIAL NOT NULL PRIMARY KEY,\n"
               "    photo VARCHAR(200),\n"
               "    first_name VARCHAR(30) NOT NULL,\n"
               "    last_name VARCHAR(30) NOT NULL,\n"
               "    patronymic VARCHAR(30) NOT NULL,\n"
               "    post VARCHAR(200),\n"
               "    date_of_birth VARCHAR(20),\n"
               "    phone VARCHAR(30),\n"
               "    fax VARCHAR(30),\n"
               "    email VARCHAR(100),\n"
               "    address VARCHAR(250)\n"
               ");\n")

def insert_for_db(photo, first_name, last_name, patronymic, post, date_of_birth, phone, fax, email, address):
    with open('text_for_db.sql', mode='a', encoding='utf-8') as file:
        file.write(f"insert into db_leaders (photo, first_name, last_name, patronymic, post, date_of_birth, phone, fax, email, address) values ('{photo}', '{first_name}', '{last_name}', '{patronymic}', '{post}', '{date_of_birth}', '{phone}', '{fax}', '{email}', '{address}');\n")
