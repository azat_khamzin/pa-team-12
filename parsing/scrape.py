# pip install buautifulsoup
# pip install lxml
import re
from urllib.request import urlopen
from bs4 import BeautifulSoup
from database.script_for_fill_db import insert_for_db


def create_url_of_leaders():
    with open('leaders_rt.txt') as file:
        leaders_list = file.readlines()
    leaders_list = [x.strip() for x in leaders_list]
    return leaders_list


def get_photo(soup):
    photo = soup.find('img', class_='bio__photo')
    if photo != None:
        link = "https:" + photo.get("src")
        return link
    else:
        return None


def get_full_name(soup):
    full_name = ''
    list_names = soup.find_all('div', class_='bio__person-name')
    for name in list_names:
        full_name += name.text + ' '
    full_name.strip()
    return split_full_name(full_name)


def split_full_name(full_name):
    return full_name.split(' ')


def get_post(soup):
    check_post = soup.find('div', class_='bio__person-post')
    if check_post != None:
        return check_post.text.strip()
    else:
        return None


def check_and_get_birthday(soup):
    check_birthday = soup.find('div', class_='bio__key', text=re.compile('Дата рождения'))
    if check_birthday != None:
        birth_day = check_birthday.find_next()
        return birth_day.text.strip()
    else:
        return None


def check_and_get_phone(soup):
    check_phone = soup.find('div' , class_='bio__key', text=re.compile('Телефон'))
    if check_phone != None:
        phone = check_phone.find_next()
        return phone.text.strip()
    else:
        return None


def check_and_get_fax(soup):
    check_fax = soup.find('div', class_='bio__key', text=re.compile('Факс'))
    if check_fax != None:
        fax = check_fax.find_next()
        return fax.text.strip()
    else:
        return None


def check_and_get_email(soup):
    check_mail = soup.find('div', class_='bio__key', text=re.compile('E-Mail'))
    if check_mail != None:
        mail = check_mail.find_next()
        return mail.text.strip()
    else:
        return None


def check_and_get_address(soup):
    check_address = soup.find('div', class_='bio__key', text=re.compile('Адрес'))
    if check_address != None:
        address = check_address.find_next()
        return address.text.strip()
    else:
        return None


def check_and_get_biography(soup):
    check_biography = soup.find('div', class_='bio__text')
    if check_biography != None:
        return check_biography.text.strip()
    else:
        return None


def check_and_get_more_info(soup):
    list_of_more_info = []
    check_more_info = soup.find('div', class_='bio__h2 h2 h2--upper')
    if check_more_info != None:
        all_links = check_more_info.find_next().find_all('a')
        for item in all_links:
            item_text = item.text
            item_url = item.get('href')
            list_of_more_info.append(f"{item_text}: {item_url}")
        if list_of_more_info != []:
            return list_of_more_info
        else:
            return None
    else:
        return None


def get_normal_format_of_birth_day(birth_day):
    dict_months = {
        'января':'01',
        'февраля':'02',
        'марта':'03',
        'апреля':'04',
        'мая':'05',
        'июня':'06',
        'июля':'07',
        'августа':'08',
        'сентября':'09',
        'октября':'10',
        'ноября':'11',
        'декабря':'12'
    }
    list_of_birth_day = birth_day.strip().split(' ')
    good_date = list_of_birth_day[0] + '/'
    for key in dict_months:
        if key in birth_day:
            good_date += dict_months[key] + '/'
    good_date += list_of_birth_day[2]
    return good_date


leaders_url = create_url_of_leaders()
# https://minzdrav.tatarstan.ru/rukov/minister.htm - good(example1)
# https://minzdrav.tatarstan.ru/upravlyayushchiy-delami.htm - bad(example2)
#leaders_url = ['https://minzdrav.tatarstan.ru/upravlyayushchiy-delami.htm']


for url in leaders_url:
    url = urlopen(url)
    soup = BeautifulSoup(url, 'lxml')

    photo = get_photo(soup)
    print('Photo:', photo)

    full_name = get_full_name(soup)
    first_name = full_name[0]
    last_name = full_name[1]
    patronymic = full_name[2]
    print('First_name:', first_name)
    print('Last_name:', last_name)
    print('Patronymic:', patronymic)

    post = get_post(soup)
    print('Post:', post)

    birth_day = check_and_get_birthday(soup)
    if birth_day != None:
         birth_day = get_normal_format_of_birth_day(birth_day)
    print('Birth day:', birth_day)

    phone = check_and_get_phone(soup)
    print('Phone:', phone)

    fax = check_and_get_fax(soup)
    print('FAX:', fax)

    email = check_and_get_email(soup)
    print('Email:', email)

    address = check_and_get_address(soup)
    print('Address:', address)

    biography = check_and_get_biography(soup)
    print('Biography:\n', biography)

    more_info = check_and_get_more_info(soup)
    print('More info:', more_info)

    insert_for_db(photo, first_name, last_name, patronymic, post, birth_day, phone, fax, email, address)

    print()
#prints are for example of data
